import random
import numpy as np
import pandas as pd
from operator import add
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
from keras.utils import to_categorical
from collections import deque
from names import get_random_name

class DQNAgent:
    def __init__(self, state_size, action_size, memory_size=2000, replay_batch_size=32, gamma=.95, epsilon=1, epsilon_min=0.01, epsilon_decay=0.995, learning_rate=0.001):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=memory_size)
        self.gamma = gamma   # discount rate
        self.epsilon = epsilon  # exploration rate
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay
        self.learning_rate = learning_rate
        self.replay_batch_size = replay_batch_size
        self.name = get_random_name()
        self.model = self.build_model()

    # Build the NN for the Deep Q Agent
    def build_model(self):
        model = Sequential()

        # Add layers
        model.add(Dense(24, input_dim=self.state_size, activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

        return model

    # Append the state-action-reward-state chain to the agent's memory
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    # Get the recommended action
    def act(self, state):
        # Random or predicted?
        if np.random.rand() <= self.epsilon:
            # Random
            return random.randrange(self.action_size)
        # Predicted
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    # Look through memories and learn from them
    def replay(self):
        # Get memory batch
        if self.replay_batch_size > len(self.memory):
            minibatch = list(self.memory)
        else:
            minibatch = random.sample(self.memory, self.replay_batch_size)

        # Learn from memory batch
        for state, action, reward, next_state, done in minibatch:
            target = reward
            # Get predicted Q-value
            if not done:
              target = reward + self.gamma * \
                       np.amax(self.model.predict(next_state)[0])

            # Map state to predicted reward
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)

        # Decay epsilon (less random)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
