import gym
import numpy as np
from dqn_agent import DQNAgent

# Initialize DQN agent
agent = DQNAgent(4, 2, replay_batch_size=50)

# Create the OpenAI Gym environment
env = gym.make('CartPole-v0')
env._max_episode_steps = 500

# Play the game
print(agent.name)
for eps in range(1000):
    # Reset game, get starting state
    state = env.reset()
    state = np.reshape(state, [1,4])

    # For each game frame
    for t in range(5000):
        # Render the game
        env.render()

        # Get the recommended action from the model
        action = agent.act(state)

        # Get the new state
        next_state, reward, done, info = env.step(action)
        next_state = np.reshape(next_state, [1,4])

        # agent.train_short_memory(this_observation, action, reward, next_observation)
        # Add this Markov step to the model's memory
        agent.remember(state, action, reward, next_state, done)

        if done:
            print('Game: ' + str(eps) + '\tScore: ' + str(t))
            break

        state = next_state

    agent.replay()

env.close()
